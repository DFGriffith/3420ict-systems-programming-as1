#include <stdio.h> 
#include <stdbool.h>
#include <string.h>    
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h> 
 
#include <windows.h>

#include <fcntl.h>
#include <limits.h>


// Global variables
char arg1[256];
char arg2[256];
char rd_cmd[256];
char rd_arg[256];
int fflag = 0;
int lflag = 0;

char message[1000];

int sock;
struct sockaddr_in server;    

LARGE_INTEGER frequency;   // ticks per second
LARGE_INTEGER t1, t2;      // ticks
double elapsedTime;

pid_t pid;
int status;


// Method declarations
int runClient();
void setArgs(char*);
int getQuery(char*);
int listQuery(char*);
int putQuery();
int sendrecv();
void zombieKill();


int main(int argc, char *argv[])
{
    arg1[0] = 0;
    arg2[0] = 0;
    rd_cmd[0] = 0;
    rd_arg[0]= 0;
    
    atexit(zombieKill);

    // get ticks per second
    QueryPerformanceFrequency(&frequency); 

    //-- Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons( 80 );
 

    //-- Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Connected\n");


    // // set non blocking
    // int flags; 

    // if ((flags = fcntl(sock, F_GETFL, 0)) < 0) 
    // { 
    //     perror("fcntl F_GETFL");
    //     exit(1);
    // } 

    // if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0) 
    // { 
    //     perror("fcntl F_SETFL");
    //     exit(1);
    // } 
     

    //-- Keep communicating with server
    runClient();
     

    close(sock);
    return 0;
}



int runClient()
{

    while(1)
    {
        printf("Enter message : ");
        //scanf("%s" , message);
        scanf(" %[^\t\n]",message);

        // Quit client
        if(strcmp(message, "quit") == 0) 
        {
            // write
            close(sock);
            exit(0);
        }


        printf("m1... %s\n", message); // debug

        // Send some data
        if( send(sock , message , strlen(message) , 0) < 0)
        {
            puts("Send failed");
            close(sock);
            exit(1);
        }

        // start timer
        QueryPerformanceCounter(&t1);


        // Fork to recv
        if ((pid = fork()) == -1)
        {
            perror("fork");
            close(sock);
            continue;
        }
        else if(pid == 0)
        {     
            sendrecv(message);

            close(sock);
        }
        
        memset(message, '\0', strlen(message));

        printf("m2... %s\n", message);
        sleep(1);

    }
}



int sendrecv(char* message)   // Currently only recving
{
    char server_reply[8000];

    int iResult = 0;

    // // Receive until the peer closes the connection
    // do {

    //     iResult = recv(sock, server_reply, sizeof(server_reply), 0);

    //     if ( iResult > 0 )
    //     {
    //         // stop timer & compute + print the elapsed time in millisec
    //         QueryPerformanceCounter(&t2);
    //         elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

    //         puts("\nTime elapsed :");
    //         printf("%G ms\n", elapsedTime);
             

    //         // print server reply
    //         puts("Server reply :");
    //         puts(server_reply);


    //         // check whether to write "get" || "list" to file
    //         char* query;
    //         setArgs(query);

    //         if(strcmp(query, "get") == 0)
    //         {
    //             getQuery(server_reply);
    //         }
    //         else if(strcmp(query, "list") == 0)
    //         {
    //             listQuery(server_reply);
    //         }

    //         memset(server_reply, '\0', strlen(server_reply));
    //         memset(message, '\0', strlen(message));
    //         memset(query, '\0', strlen(query));

    //         fflush(stdout);
    //     }
    //     else if ( iResult == 0 )
    //         printf("Connection closed\n");
    //     else
    //         perror("recv");

    // } while( iResult > 0 );



    //Receive a reply from the server
    if( recv(sock , server_reply , sizeof(server_reply) , 0) < 0)
    {
        // if (errno != EWOULDBLOCK)
        // {
            perror("recv");
            puts("recv failed");
            close(sock);
            exit(1);
        // }
    }
    else
    {
        // stop timer & compute + print the elapsed time in millisec
        QueryPerformanceCounter(&t2);
        elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

        puts("\nTime elapsed :");
        printf("%G ms\n", elapsedTime);

        puts("Server reply :");
         
        // check whether to write "get" || "list" to file
        char* query;
        setArgs(query);

        printf("%s\n", query);
        
        if(strcmp(query, "put") == 0)
        {
            if(strcmp(server_reply, "ready") == 0 && arg1[0] != 0)
            {
                putQuery();
            }
            else if(strcmp(server_reply, "error") == 0)
            {
                printf("Put server error\n");
            }
            else if(strcmp(server_reply, "file exists") == 0)
            {
                printf("File already exists on server (use '-f' to overwrite)\n");
            }

        }
        else if(strcmp(query, "get") == 0)
        {
            if (arg2[0] != 0)
            {
                getQuery(server_reply);
            }
            else if(rd_cmd[0] != 0 && rd_arg[0] != 0)
            {
                int fd;
                char template[] = "/tmp/myfileXXXXXX";
                char fname[PATH_MAX];

                strcpy(fname, template);  // Copy template 

                fd = mkstemp(fname);      // Create and open temp file 

                write(fd, server_reply, strlen(server_reply));
                
                lseek(fd, 0L, SEEK_SET);  // Rewind to front 
                
                

                dup2(fd, STDIN_FILENO);
                

                char tmp[52];
                strcpy(tmp, "/bin/"); 
                strcat(tmp, rd_cmd);
                
                if (execl(tmp, rd_cmd, rd_arg, NULL) < 0)
                { 
                    perror("exec");
                    return 1;
                }

                close(fd);  
                unlink(fname);  // Remove file
            }
            else
            {
                puts(server_reply);
            }
        }
        else if(strcmp(query, "list") == 0)
        {
            if(arg2[0] != 0)
            {
                listQuery(server_reply);
            }
            else if(arg1[0] != 0 && strchr(arg1, '/') == NULL)
            {
                listQuery(server_reply);
            }
            else
            {
                puts(server_reply);
            }
        }
        else
        {
            // print server reply
            puts(server_reply);
        }

        memset(arg1, '\0', strlen(arg1));
        memset(arg2, '\0', strlen(arg2));
        memset(server_reply, '\0', strlen(server_reply));
        memset(message, '\0', strlen(message));
        memset(query, '\0', strlen(query));

        fflush(stdout);
    }

    exit(0);
}




int putQuery()
{
    char fileData[8192];

    FILE *fp = fopen(arg1, "r");

    if(fp == NULL)
    {
        perror("Error with filename argument..\n");
        return 1;
    }

    char line[60];
    char *p;


    if(p = fgets(line, 60, fp))
    {
        strcpy(fileData, p);
    }
    else
    {
        perror("File read erro");
        return 1;
    }

    while (p = fgets(line, 60, fp)) 
    {
        strcat(fileData, p);
    }
    //printf(fp, "%s\n", server_reply);
    
    fclose(fp);

    // Send some data
    if( send(sock , fileData , strlen(fileData) , 0) < 0)
    {
        puts("Send failed");
        close(sock);
        exit(1);
    }

    return 0;    
}




int listQuery(char* reply)
{
    char fname[100], server_reply[1000];
    strcpy(server_reply, reply);

    if(arg2[0] != 0)
    {
        memcpy(fname, arg2, strlen(arg2));
    }
    else if(arg1[0] != 0 && strchr(arg1, '/') == 0)
    {
        memcpy(fname, arg1, strlen(arg1));
    }

    if(fname[0] != 0)
    {
        // check if file exists
        if( access( fname, F_OK ) != -1 ) 
        {
            if(fflag != 1)
            {
                printf("Output file already exist..\n");
                return 1;
            }

        } 

        printf("List copied to file...\n");
        // printf("%s\n", server_reply);

        FILE *fp;

        fp = fopen(fname, "w");
        if(fp == NULL)
        {
            perror("Error with filename argument..\n");
            return 1;
        }

        fprintf(fp, "%s\n", server_reply);
        
        fclose(fp);

        return 0;

    }

    printf("error with file name arg..\n");
    return 1;
}




int getQuery(char* reply)
{
    char server_reply[1000];
    strcpy(server_reply, reply);

    // if file name was given
    if(arg2[0] != 0)
    {
        // check if file exists
        if( access( arg2, F_OK ) != -1 ) 
        {
            if(fflag != 1)
            {
                printf("Output file already exist..\n");
                return 1;
            }
        } 


        FILE *fp;
        fp = fopen(arg2, "w");

        if(fp == NULL)
        {
            perror("Error with filename argument..\n");
            return 1;
        }

        fprintf(fp, "%s\n", server_reply);

        printf("Get cmd copied to file...\n");

        fclose(fp);
        return 0;
    }
}




void setArgs(char* query)
{
    char* tmp;

    // Get query
    query = strtok(message, " ");

    // Check for arguments
    tmp = strtok(NULL, " ");
    while( tmp )
    {
        if( strcmp(tmp, "-l") == 0)
        {
            lflag = 1;
        }
        else if (strcmp(tmp, "-f") == 0)
        {
            fflag = 1;
        }
        else if(strlen(arg1) == 0)
        {
            strcpy(arg1, tmp);
        }
        // check for out redirection arguments
        else if(strcmp(tmp, "|") == 0)       
        {
            tmp = strtok(NULL, " ");
            if(tmp)
            {
                strcpy(rd_cmd, tmp);
            }
            else
            {
                printf("no redirection cmd\n");
            }

            if(strcmp(query, "get") == 0)
            {
                tmp = strtok(NULL, " ");
                if(tmp)
                    strcpy(rd_arg, tmp);
                else
                    printf("no redirection argument\n");
            }
        }
        else if(rd_arg[0] == 0 && strlen(arg2) == 0)
        {
            printf("arg2\n");
            strcpy(arg2, tmp);
        }  

        tmp = strtok(NULL, " ");
    } 
}




// On exit function
void zombieKill()
{
    // // wait for child
    if (waitpid(pid, &status, 0) < 0) 
    {
        printf("No Zombies!..\n\n");
    } 
}

