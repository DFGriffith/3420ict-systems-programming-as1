//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//               Server - queries.h                 //
//                                                  //
//--------------------------------------------------//

#ifndef QUERIES_H
#define QUERIES_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>

#include "dirent.h"
#include <sys/utsname.h>

#include <sys/types.h> 
#include <sys/stat.h>

#include <pwd.h>


#define BUF_SIZE 8192


// Global variables
int socket_desc , client_sock , c , read_size;
char* query;
pid_t pid;
int status;


// Methods declarations
void delayQuery(char*);

int putQuery(char*, char*, int);

int getQuery(char*);

int listQuery(char*, int);
int longlist(struct dirent *ent, char* out); // Sets out to a file's long list information

int sysQuery();
char* cpuInfo(char *); // Returns CPU info for sysQuery


#endif