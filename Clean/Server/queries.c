//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//               Server - queries.c                 //
//                                                  //
//--------------------------------------------------//

#include "queries.h"


//--------------------------------------------//
//           Server - Delay query             //
//--------------------------------------------//

void delayQuery(char* arg)
{
    int delaySeconds;
    sscanf(arg, "%d", &delaySeconds);

    if(delaySeconds > 0)
    {
        sleep(delaySeconds);
        write(client_sock, arg, strlen(arg));
    }
    else
    {
        write(client_sock, "delay time error..\n", 19);
    }
}


//--------------------------------------------//
//             Server - Put query             //
//--------------------------------------------//

int putQuery(char* arg1, char* arg2, int fflag)
{
    char fname[100];
    char buffer[BUF_SIZE];

    // Check if new and set file name 
    if(arg2[0] != 0)
    {
        strcpy(fname, arg2);
    }
    else
    {
        strcpy(fname, arg1);
    }


    // check if file exists
    if( access( fname, F_OK ) != -1 ) 
    {
        if(fflag != 1)
        {
            printf("Output file exists (no overwrite)\n");
            write(client_sock, "file exists", 12);
            return 1;
        }
    } 


    // Open file
    FILE *fp;
    fp = fopen(fname, "w");

    if(fp == NULL)
    {
        perror("Error with filename argument..");
        write(client_sock, "error", 6);

        fclose(fp);
        return 1;
    }


    write(client_sock, "ready", 6); // tell client we are ready to recv

    recv(client_sock , buffer , 2000 , 0); // recv file data


    fprintf(fp, "%s\n", buffer); // print data to file

    write(client_sock, "Put query done..\n", 17);
 
    fclose(fp);
    return 0;
}


//--------------------------------------------//
//             Server - Get query             //
//--------------------------------------------//

int getQuery(char* arg1)
{
    char buffer[BUF_SIZE];    
 

    // Check for file name
    if(arg1[0] == 0) 
    {
        write(client_sock, "No file specified..\n", 20);
        return 1;
    }


    // Open file for reading
    FILE* fp = fopen(arg1, "r"); 
    if(fp == NULL)
    {
        perror("Error with filename argument..\n");
        write(client_sock, "Error with filename argument..\n", 31);
        return 1;
    } 
 
    // Write file content (40 lines at a time)
    int count = 0;
    char* p;
    char line[60];

    while (p = fgets(line, 60, fp)) 
    {
        if(count <= 40)     // Check how many lines have been printed
        {
            write(client_sock, p, strlen(p));
        }
        else                // If count is greater than 40 wait for user response before continuing 
        {
            count = 0;
            char* ctnd = "Type and enter for next...\n";
            write(client_sock, ctnd, strlen(ctnd));

            recv(client_sock , buffer , 2000 , 0); // wait for client response
        }

        count++;  
    }

    fclose(fp);
    return 0;   
}


//--------------------------------------------//
//             Server - List query            //
//--------------------------------------------//

int listQuery(char* arg1, int lflag)
{
    char path[1024], buffer[BUF_SIZE];;
                
    DIR *dir;
    struct dirent *ent;

    // check for path argument and set path
    if(strchr(arg1, '/') != 0) //arg1[0] != 0 && 
    {
        memcpy(path, arg1, strlen(arg1));
    }
    // else set path to cwd
    else if (getcwd(path, sizeof(path)) == NULL)
    {
        perror("getcwd error : ");
        write(client_sock, "list error", 10);
        return 1;
    }

    // Try to open path directory
    if ((dir = opendir (path)) != NULL) 
    {
        int bytesSent = 0, check = 0, count = 0;

        // Loop through files 
        while ((ent = readdir (dir)) != NULL) 
        {
            if(count <= 40) // Check how many lines have been printed 
            {
                // If "." or ".." skip
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) 
                {
                    continue;
                }
                else
                {   
                    // if long list "-l" get file info
                    if(lflag == 1)
                    {
                        char out[100];
                        longlist(ent, out); // Set out with file info
                        write(client_sock, out, strlen(out));
                    }
                    // else return file name
                    else          
                    {
                        write(client_sock, ent->d_name , strlen(ent->d_name));
                        write(client_sock, "\n" , 1);
                    }
                }
            }
            else // If count is greater than 40 wait for user response before continuing
            {
                count = 0;
                char* ctnd = "\nType to continue...\n";
                write(client_sock, ctnd, strlen(ctnd));

                recv(client_sock , buffer , 2000 , 0); // wait for client response
            }

            count++;

        }
        closedir (dir);
    }
    else
    {
        perror("Opening dir error : ");
        write(client_sock, "Error opening directory..\n", 26);
    }

    return 0;
}


int longlist(struct dirent *ent, char* str)
{
    struct stat stbuf;

    // Get file stats
    if (stat(ent->d_name, &stbuf) == -1) 
    {
        perror("File stat error");
        return 1;
    }

    char out[100], sizeVal[100], owner[100], modtime[100];

    // Add file name
    strcpy(out, ent->d_name);
    strcat(out, " | Size: ");

    // Add size
    snprintf (sizeVal, sizeof(sizeVal), "%d", stbuf.st_size);
    strcat(out, sizeVal);
    strcat(out, "bytes | Owner: ");

    // Add owner name
    struct passwd *pw;
    pw = getpwuid(stbuf.st_uid);
    if (!pw)
    {
        perror("struct passwd");
    }
    else
    {
        if(pw->pw_name != NULL)
        {
            snprintf (owner, sizeof(owner), "%s", pw->pw_name);
            strcat(out, owner);
        }
    }

    // Add date
    strcat(out, " | Date: ");
    strftime(modtime, sizeof(modtime), "%Y-%m-%d", localtime(&stbuf.st_mtime));
    strcat(out, modtime);

    // Add access permissions
    strcat(out, (S_ISDIR(stbuf.st_mode)) ? " | Access: d" : " | Access: -");
    strcat(out, (stbuf.st_mode & S_IRUSR) ? "r" : "-");
    strcat(out, (stbuf.st_mode & S_IWUSR) ? "w" : "-");
    strcat(out, (stbuf.st_mode & S_IXUSR) ? "x" : "-");
    strcat(out, (stbuf.st_mode & S_IRGRP) ? "r" : "-");
    strcat(out, (stbuf.st_mode & S_IWGRP) ? "w" : "-");
    strcat(out, (stbuf.st_mode & S_IXGRP) ? "x" : "-");
    strcat(out, (stbuf.st_mode & S_IROTH) ? "r" : "-");
    strcat(out, (stbuf.st_mode & S_IWOTH) ? "w" : "-");
    strcat(out, (stbuf.st_mode & S_IXOTH) ? "x ~\n" : "- ~\n\0");


    strcpy(str, out); // Copy to arg

    return 0;
}


//--------------------------------------------//
//             Server - Sys query             //
//--------------------------------------------//

int sysQuery()
{
    char out[50];

    // Get system & OS info
    struct utsname unameData;
    uname(&unameData);

    char sys[100];
    strcpy(sys, "System : ");
    strcat(sys, unameData.sysname);

    char ver[100];
    strcpy(ver,"\nVersion : ");
    strcat(ver, unameData.version);

    // Get CPU info
    char cpu[50];
    cpuInfo(cpu);  

    // Combine and write info
    strcpy(out, sys);
    strcat(out, ver);
    strcat(out, cpu);

    write(client_sock, out, strlen(out));

    return 0;
}


char* cpuInfo(char* str)
{
    char info[100];

    // Open CPU info file for reading
    FILE *cpuinfo = fopen("/proc/cpuinfo", "r");

    char* key = "model name"; // File search key
    char line[60];
    char *p;
    int findlen = strlen(key);

    // Check each line until "model name" is found
    while (p = fgets(line, 60, cpuinfo)) 
    {
        if(strstr(p, key) != NULL)
        {   
            p += (findlen + 2);

            break;
        }
    }

    fclose(cpuinfo);


    strcpy(info,"\nCPU :");
    strcat(info, p);


    strcpy(str, info); // copy info to arg

    return str;
}




