//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//                 Server - main.c                  //
//                                                  //
//--------------------------------------------------//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>    
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h> 
#include <errno.h>

#include "queries.h"
// #include <fcntl.h>

#define BUF_SIZE 8192



//--------------------------------------------//
//         Server - Global variables          //
//--------------------------------------------//

extern int socket_desc , client_sock , c , read_size;
extern char* query;
extern pid_t pid;
extern int status;
char client_message[BUF_SIZE];
struct sockaddr_in server , client;



//--------------------------------------------//
//        Server - Method Declarations        //
//--------------------------------------------//

int runServer();
int recvMessage();
int handleQuery(char*, char*, int, int);
void zombieKill();



//--------------------------------------------//
//            Server - Main Method            //
//--------------------------------------------//

int main(int argc , char *argv[])
{  
    // Set at exit function
    atexit(zombieKill);


    // Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");


    // Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 80 );
     

    // Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        close (client_sock);
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     

    // Listen
    listen(socket_desc , 10);


    // Run server application loop
    runServer();


    close (client_sock);
    return 0;
}



//--------------------------------------------//
//      Server - Server Application Flow      //
//--------------------------------------------//

int runServer()
{
    while(1)
    {
        puts("Waiting for incoming connections...");
        c = sizeof(struct sockaddr_in);


        // Accept connection from an incoming client
        client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
        if (client_sock < 0)
        {
            close (client_sock);
            perror("accept failed");
            return 1;
        }
        puts("Connection accepted");


        // Fork to handle connection and listen for more clients
        if ((pid = fork()) == -1)
        {
            perror("fork");
            close(client_sock);
            continue;
        }
        else if(pid == 0)
        {
            recvMessage(); // Receive a message from client
            close(client_sock);
            break;
        }


        // Check for  errors
        else if(read_size == -1)
        {
            perror("recv failed");
        }

        fflush(stdout);

        close (client_sock);
    }

    return 0;
}



//--------------------------------------------//
//   Server - Recieve messages from clients   //
//--------------------------------------------//

int recvMessage()
{
    char clmsg[BUF_SIZE];

    while( (read_size = recv(client_sock , clmsg , BUF_SIZE , 0)) > 0 )
    {
        strcpy(client_message, clmsg);
        char* tmp;
        int lflag = 0;
        int fflag = 0;
        char arg1[256] = {0};
        char arg2[256] = {0};
        char rd_arg[256] = {0};
        char rd_cmd[256] = {0};


        // Check for flags
        if(strstr(client_message, "-l") != NULL)
            lflag = 1;
        if(strstr(client_message, "-f") != NULL)
            fflag = 1;


        // Get query
        query = strtok(client_message, " ");


        // Check for arguments
        tmp = strtok(NULL, " ");
        while( tmp )
        {
            if(strlen(arg1) == 0)                           // check for arg1
            {
                strcpy(arg1, tmp);
            }
            else if(strcmp(tmp, "|") == 0)                  // check for out redirection cmd & argument
            {
                tmp = strtok(NULL, " ");
                if(tmp)
                {
                    strcpy(rd_cmd, tmp);
                }
                else
                {
                    printf("no redirection cmd\n");
                }

                // if "get" query check for redirection argument
                if(strcmp(query, "get") == 0) 
                {
                    tmp = strtok(NULL, " ");
                    if(tmp)
                        strcpy(rd_arg, tmp);
                    else
                        printf("no redirection argument\n");
                }
            }
            else if(rd_arg[0] == 0 && strlen(arg2) == 0)    // check for arg2
            {
                strcpy(arg2, tmp);
            }


            tmp = strtok(NULL, " ");
        } 


        // Debug
        printf("%s : %d : %s : %s : %d : %s\n", query, lflag, arg1, arg2, fflag, client_message);
        
        
        // Identify the query and execute it
        handleQuery(arg1, arg2, lflag, fflag); 


        memset(client_message, 0, strlen(client_message));
        memset(clmsg, 0, strlen(clmsg));
    }

    return 0;
}



//--------------------------------------------//
//         Server - Handle User Query         //
//--------------------------------------------//

int handleQuery(char* arg1, char* arg2, int lflag, int fflag)
{
    // // Quit server
    // if(strcmp(client_message, "endServer") == 0)
    // {
    //     printf("endServer.. \n");
    //     close (client_sock);
    //     exit(0);
    // }
    
    // List query
    if(strcmp(client_message, "list") == 0)
    { 
        listQuery(arg1, lflag);
    }
    // Sys query
    else if(strcmp(client_message, "sys") == 0)
    {
        sysQuery();
    }
    // Delay query
    else if(strcmp(client_message, "delay") == 0)
    {   
        delayQuery(arg1);
    }
    // Put query
    else if(strcmp(client_message, "put") == 0)
    {   
        putQuery(arg1, arg2, fflag);
    }
    // Get query
    else if(strcmp(client_message, "get") == 0)
    {   
        getQuery(arg1);
    }
    // Incorrect query
    else
    {
        char* queryErr = "incorrect query..\n";
        write(client_sock, queryErr, strlen(queryErr));
    }

    return 0;
}



//--------------------------------------------//
//          Server - onExit Function          //
//--------------------------------------------//

void zombieKill()
{
    // Wait for children
    if (waitpid(pid, &status, 0) < 0) 
    {
        printf("No Zombies!..\n");
    } 
}

