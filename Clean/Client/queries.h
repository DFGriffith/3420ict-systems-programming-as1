//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//               Client - queries.h                 //
//                                                  //
//--------------------------------------------------//

#ifndef QUERIES_H
#define QUERIES_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h>


#define BUF_SIZE 8192


// Global variables
char arg1[256];
char arg2[256];
int fflag;
int lflag;
int sock;


// Methods declarations
int getQuery(char*);

int listQuery(char*);

int putQuery();


#endif
