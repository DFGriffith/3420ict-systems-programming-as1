//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//                 Client - main.c                  //
//                                                  //
//--------------------------------------------------//

#include <stdio.h> 
#include <stdbool.h>
#include <string.h>    
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h> 
 
#include <windows.h>

#include <fcntl.h>
#include <limits.h>

#include "queries.h"



//--------------------------------------------//
//         Client - Global variables          //
//--------------------------------------------//

extern int sock;
extern char arg1[256];
extern char arg2[256];
extern int fflag;
extern int lflag;

char rd_cmd[256];          // Redirection cmd
char rd_arg[256];          // Redirection arg

struct sockaddr_in server;    
char message[1000]; 
pid_t pid;
int status;

LARGE_INTEGER frequency;   // ticks per second
LARGE_INTEGER t1, t2;      // ticks
double elapsedTime;        // time calculated with ticks



//--------------------------------------------//
//        Client - Method Declarations        //
//--------------------------------------------//

int runClient();
void setArgs(char*);
int recvReply();
int handleReply(char*, char*);
void zombieKill();



//--------------------------------------------//
//            Client - Main Method            //
//--------------------------------------------//

int main(int argc, char *argv[])
{
    arg1[0] = 0;
    arg2[0] = 0;
    fflag = 0;
    lflag = 0;
    rd_cmd[0] = 0;
    rd_arg[0]= 0;
    
    // Set at exit function
    atexit(zombieKill);


    // Get ticks per second
    QueryPerformanceFrequency(&frequency); 


    // Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    if(argc > 1)        // If address argument given
    {
        server.sin_addr.s_addr = inet_addr(argv[1]);
    }   
    else                // else use "127.0.0.1" local address
    {
   // printf("argc: %d\n", argc);
        server.sin_addr.s_addr = inet_addr("127.0.0.1");
    }
    server.sin_family = AF_INET;
    server.sin_port = htons( 80 );
 

    // Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Connected");


    /* Non blocking removed code 1 */


    // Keep communicating with server
    runClient();
     

    close(sock);
    return 0;
}



//--------------------------------------------//
//       Client - Main Application Loop       //
//--------------------------------------------//

int runClient()
{
    while(1)
    {
        // Get user query
        printf("\nEnter message : ");
        scanf(" %[^\t\n]",message);


        // Quit client
        if(strcmp(message, "quit") == 0) 
        {
            // write
            close(sock);
            exit(0);
        }


        // Send some data
        if( send(sock , message , strlen(message) , 0) < 0)
        {
            puts("Send failed");
            close(sock);
            exit(1);
        }


        // Start timer
        QueryPerformanceCounter(&t1);


        // Fork to recveive reply and handle more client queries 
        if ((pid = fork()) == -1)
        {
            perror("fork");
            close(sock);
            continue;
        }
        else if(pid == 0)
        {     
            recvReply(); // Recive reply from server
            close(sock);
        }
        
        memset(message, '\0', strlen(message));

        sleep(1); // Pause for one second
    }
}



//--------------------------------------------//
//       Client - Receive Server Reply        //
//--------------------------------------------//

int recvReply()
{
    char server_reply[8000];

    /* Non blocking removed code 2 */

    // Receive a reply from the server
    if( recv(sock , server_reply , sizeof(server_reply) , 0) < 0)
    {
        perror("recv");
        puts("recv failed");
        close(sock);
        exit(1);
        /* Non blocking removed code 3 */
    }
    else
    {
        // Stop timer, compute & print the elapsed time in millisec
        QueryPerformanceCounter(&t2);
        elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;


        // Get query and set args and flags
        char query[50];
        setArgs(query);


        // Print time
        puts("\nTime elapsed :");
        printf("%G ms\n", elapsedTime);

        puts("Server reply :");


        handleReply(query, server_reply);


        // Reset variables
        memset(arg1, '\0', strlen(arg1));
        memset(arg2, '\0', strlen(arg2));
        memset(server_reply, '\0', strlen(server_reply));
        memset(query, '\0', strlen(query));
        fflush(stdout);
    }

    exit(0);
}



//--------------------------------------------//
//        Client - Handle Server Reply        //
//--------------------------------------------//

int handleReply(char* query, char* server_reply)
{
    // If "put" query check whether to server is ready for file 
    if(strcmp(query, "put") == 0)                
    {
        if(strcmp(server_reply, "ready") == 0 && arg1[0] != 0)  // Server reply ready
        {
            putQuery();
        }
        else if(strcmp(server_reply, "error") == 0)             // Server reply error
        {
            printf("Put server error\n");
        }
        else if(strcmp(server_reply, "file exists") == 0)       // Server reply file exists
        {
            printf("File already exists on server (use '-f' to overwrite)\n");
        }

    }
    // Else if "get" query check whether to write file to or redirect 
    else if(strcmp(query, "get") == 0)        
    {

        if (arg2[0] != 0)                           // If arg2 print to file
        {
            getQuery(server_reply);
        }
        else if(rd_cmd[0] != 0 && rd_arg[0] != 0)   // If redirection args
        {
            printf("Redirection -\n");
            // Redirect server reply to execl
            redirection(server_reply);
        }
        else                                        // Else print to screen
        {
            puts(server_reply);
        }
    }
    // Else if "list" query check whether to write to file or redirect
    else if(strcmp(query, "list") == 0)  
    {
        if(arg2[0] != 0)                                        // If arg2 print to file
        {
            listQuery(server_reply);
        }
        else if(arg1[0] != 0 && strchr(arg1, '/') == NULL)      // If arg1 doesn't have a '/' print to file
        {
            listQuery(server_reply);
        }
        else if(rd_cmd[0] != 0)                                 // If redirection arg
        {
            printf("Redirection -\n");
            // Redirect server reply to execl
            redirection(server_reply);
        }
        else                                                    // Else print to screen
        {
            puts(server_reply);
        }
    }
    // Else just print to screen
    else                                
    {
        puts(server_reply); // print server reply
    }

    return 0;
}



//--------------------------------------------//
//        Client - Output Redirection         //
//--------------------------------------------//

int redirection(char* server_reply)
{
    // Create temporary file name
    int fd;
    char template[] = "/tmp/myfileXXXXXX";
    char fname[PATH_MAX];

    strcpy(fname, template);  // Copy template name

    fd = mkstemp(fname);      // Create and open temp file 

    write(fd, server_reply, strlen(server_reply));   // Write server reply to temp file

    lseek(fd, 0L, SEEK_SET);  // Rewind to front of file
    
    
    dup2(fd, STDIN_FILENO);
    

    // Exec cmd
    char tmp[52];
    strcpy(tmp, "/bin/"); 
    strcat(tmp, rd_cmd);

    if (execl(tmp, rd_cmd, rd_arg, NULL) < 0) 
    { 
        perror("exec");
        return 1;
    }

    close(fd);  
    unlink(fname);  // Remove file
    return 0;
}



//--------------------------------------------//
//           Client - set Arguments           //
//--------------------------------------------//

void setArgs(char* query)
{
    char* tmp;
    char* qry;
    
    // Get query & copy to query argument
    qry = strtok(message, " ");
    strcpy(query, qry); 


    // Check for arguments
    tmp = strtok(NULL, " ");
    while( tmp )
    {
        if( strcmp(tmp, "-l") == 0)
        {
            lflag = 1;
        }
        else if (strcmp(tmp, "-f") == 0)
        {
            fflag = 1;
        }
        else if(strlen(arg1) == 0)
        {
            strcpy(arg1, tmp);
        }
        // check for out redirection arguments
        else if(strcmp(tmp, "|") == 0)       
        {
            tmp = strtok(NULL, " ");
            if(tmp)
            {
                strcpy(rd_cmd, tmp);
            }
            else
            {
                printf("no redirection cmd\n");
            }

            if(strcmp(query, "get") == 0)
            {
                tmp = strtok(NULL, " ");
                if(tmp)
                    strcpy(rd_arg, tmp);
                else
                    printf("no redirection argument\n");
            }
        }
        else if(rd_arg[0] == 0 && strlen(arg2) == 0)
        {
            strcpy(arg2, tmp);
        }  

        tmp = strtok(NULL, " ");
    } 
}



//--------------------------------------------//
//          Client - onExit function          //
//--------------------------------------------//

void zombieKill()
{
    // // wait for child
    if (waitpid(pid, &status, 0) < 0) 
    {
        printf("No Zombies!..\n");
    } 
}






/* Non blocking removed code 1 */

// set non blocking
// int flags; 
// int iResult = 0;

// if ((flags = fcntl(sock, F_GETFL, 0)) < 0) 
// { 
//     perror("fcntl F_GETFL");
//     exit(1);
// } 

// if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0) 
// { 
//     perror("fcntl F_SETFL");
//     exit(1);
// } 



/* Non blocking removed code 2 */

// Receive until the peer closes the connection
// do {

//     iResult = recv(sock, server_reply, sizeof(server_reply), 0);

//     if ( iResult > 0 )
//     {
//         // stop timer & compute + print the elapsed time in millisec
//         QueryPerformanceCounter(&t2);
//         elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

//         puts("\nTime elapsed :");
//         printf("%G ms\n", elapsedTime);
         

//         // print server reply
//         puts("Server reply :");
//         puts(server_reply);


//         // check whether to write "get" || "list" to file
//         char* query;
//         setArgs(query);

//         if(strcmp(query, "get") == 0)
//         {
//             getQuery(server_reply);
//         }
//         else if(strcmp(query, "list") == 0)
//         {
//             listQuery(server_reply);
//         }

//         memset(server_reply, '\0', strlen(server_reply));
//         memset(message, '\0', strlen(message));
//         memset(query, '\0', strlen(query));

//         fflush(stdout);
//     }
//     else if ( iResult == 0 )
//         printf("Connection closed\n");
//     else
//         perror("recv");

// } while( iResult > 0 );




/* Non blocking removed code 3 */

// if (errno != EWOULDBLOCK)
// {
    // perror("recv");
    // puts("recv failed");
    // close(sock);
    // exit(1);
// }
