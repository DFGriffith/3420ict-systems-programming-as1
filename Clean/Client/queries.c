//--------------------------------------------------//
//                                                  //
//   Assinment 1 Option B - Client Server System    //
//                                                  //
//            David Farrow, s2917529                //                      
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//               Client - queries.c                 //
//                                                  //
//--------------------------------------------------//

#include "queries.h"



//--------------------------------------------//
//             Client - Put query             //
//--------------------------------------------//

int putQuery()
{
    char fileData[8192];

    // Open file for reading
    FILE *fp = fopen(arg1, "r");

    if(fp == NULL)
    {
        perror("Error with filename argument..\n");
        return 1;
    }


    // Get file data
    char line[60];
    char *p;

    if(p = fgets(line, 60, fp))
    {
        strcpy(fileData, p);
    }
    else
    {
        perror("File read erro");
        return 1;
    }

    while (p = fgets(line, 60, fp)) 
    {
        strcat(fileData, p);
    }
    
    fclose(fp);


    // Send file to server
    if( send(sock , fileData , strlen(fileData) , 0) < 0)
    {
        puts("Send failed");
        close(sock);
        exit(1);
    }

    puts("Put file to server\n");

    return 0;    
}


//--------------------------------------------//
//             Client - Get query             //
//--------------------------------------------//

int getQuery(char* reply)
{
    char server_reply[1000];
    strcpy(server_reply, reply);

    // if file name was given
    if(arg2[0] != 0)
    {
        // check if file exists
        if( access( arg2, F_OK ) != -1 ) 
        {
            if(fflag != 1)
            {
                printf("Output file already exist..\n");
                return 1;
            }
        } 

        // Create/Open file for writing
        FILE *fp;
        fp = fopen(arg2, "w");

        if(fp == NULL)
        {
            perror("Error with filename argument..\n");
            return 1;
        }

        // Write data to file
        fprintf(fp, "%s\n", server_reply); 
        fclose(fp);

        printf("Get query copied to file...\n");

        return 0;
    }
}


//--------------------------------------------//
//             Client - List query            //
//--------------------------------------------//

int listQuery(char* reply)
{
    char fname[100], server_reply[1000];
    strcpy(server_reply, reply);

    if(arg2[0] != 0)                                    // If arg2 use it as file name
    {
        memcpy(fname, arg2, strlen(arg2));
    }
    else if(arg1[0] != 0 && strchr(arg1, '/') == 0)     // Else If arg1 doesn't have a '/' use arg1 as file name
    {
        memcpy(fname, arg1, strlen(arg1));
    }


    if(fname[0] != 0)
    {
        // check if file already exists
        if( access( fname, F_OK ) != -1 ) 
        {
            if(fflag != 1)
            {
                printf("Output file already exist..\n");
                return 1;
            }

        } 


        // Create/Open file for writing
        FILE *fp;

        fp = fopen(fname, "w");
        if(fp == NULL)
        {
            perror("Error with filename argument..\n");
            return 1;
        }

        // Write data to file
        fprintf(fp, "%s\n", server_reply);
        fclose(fp);


        printf("List copied to file...\n");
        return 0;
    }

    printf("error with file name arg..\n");
    return 1;
}

