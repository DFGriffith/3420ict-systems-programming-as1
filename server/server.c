#include <stdio.h>
#include <stdlib.h>
#include <string.h>    
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h> 

#include <fcntl.h>
#include <errno.h>

#include "dirent.h"
#include <sys/utsname.h>

#include <sys/types.h> 
#include <sys/stat.h>

#include <pwd.h>


#define BUF_SIZE 8192
 

 // method declarations
int runServer();
int listQuery(char*, char*, int);
char* cpuInfo(char *);
void sysQuery();
void delayQuery(char*);
int putQuery(char*, char*, int);
int getQuery(char*, char*);
void zombieKill();


// Global variables
int socket_desc , client_sock , c , read_size;
struct sockaddr_in server , client;
char client_message[2000];

char* query;

pid_t pid;
int status;

int main(int argc , char *argv[])
{  
    atexit(zombieKill);

    // Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");


    // Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 80 );
     

    // Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        close (client_sock);
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     
    // Listen
    listen(socket_desc , 10);


    // Run main application
    runServer();


    close (client_sock);
    return 0;
}



// Main application flow
int runServer()
{
    while(1)
    {
        // Accept and incoming connection
        puts("Waiting for incoming connections...");
        c = sizeof(struct sockaddr_in);

        // Accept connection from an incoming client
        client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);

        if (client_sock < 0)
        {
            close (client_sock);
            perror("accept failed");
            return 1;
        }
        puts("Connection accepted");


        if ((pid = fork()) == -1)
        {
            perror("fork");
            close(client_sock);
            continue;
        }
        else if(pid == 0)
        {
            // Receive a message from client
            recvMessage();
            
            close(client_sock);
            break;
        }


        fflush(stdout);

        if(read_size == 0)
        {
            puts("Client disconnected");
            fflush(stdout);
            close (client_sock);
        }
        else if(read_size == -1)
        {
            fflush(stdout);
            perror("recv failed");
        }

        close (client_sock);
    }

    return 0;
}



// Recieve messages from clients
int recvMessage()
{
    while( (read_size = recv(client_sock , client_message , 2000 , 0)) > 0 )
    {
        char* tmp;
        
        int lflag = 0;
        int fflag = 0;
        char arg1[256];
        char arg2[256];
        char rd_arg[256];
        char rd_cmd[256];
        arg1[0] = 0;
        arg2[0] = 0;
        rd_cmd[0] = 0;
        rd_arg[0] = 0;

        // Check for flags
        if(strstr(client_message, "-l") != NULL)
            lflag = 1;
        if(strstr(client_message, "-f") != NULL)
            fflag = 1;

        // Get query
        query = strtok(client_message, " ");

        // Check for arguments
        tmp = strtok(NULL, " ");
        while( tmp )
        {
            if(strlen(arg1) == 0)
            {
                strcpy(arg1, tmp);
            }
            else if(strcmp(tmp, "|") == 0)        // check for out redirection arguments
            {
                printf("||\n");
                tmp = strtok(NULL, " ");
                if(tmp)
                {
                    strcpy(rd_cmd, tmp);
                }
                else
                {
                    printf("no redirection cmd\n");
                }

                if(strcmp(query, "get") == 0)
                {
                    printf("rd_arg\n");
                    tmp = strtok(NULL, " ");
                    if(tmp)
                        strcpy(rd_arg, tmp);
                    else
                        printf("no redirection argument\n");
                }
            }
            else if(rd_arg[0] == 0 && strlen(arg2) == 0)
            {
                printf("arg2\n");
                strcpy(arg2, tmp);
            }

            tmp = strtok(NULL, " ");
        }            

        printf("%s : %d : %s : %s : %d : %s\n", query, lflag, arg1, arg2, fflag, client_message);


        // Quit server
        if(strcmp(client_message, "endServer") == 0)
        {
            printf("endServer.. \n");
            close (client_sock);
            exit(0);
        }
        // List
        else if(strcmp(client_message, "list") == 0)
        { 
            listQuery(arg1, arg2, lflag);
        }
        // Sys
        else if(strcmp(client_message, "sys") == 0)
        {
            sysQuery();
        }
        // Delay
        else if(strcmp(client_message, "delay") == 0)
        {   
            delayQuery(arg1);
        }
        // Put
        else if(strcmp(client_message, "put") == 0)
        {   
            putQuery(arg1, arg2, fflag);
        }
        // Get
        else if(strcmp(client_message, "get") == 0)
        {   
            printf("%s |||| %s\n", arg1, arg2);
            getQuery(arg1, arg2);
        }
        // Incorrect query
        else
        {
            char* queryErr = "incorrect query..\n";
            write(client_sock, queryErr, strlen(queryErr));
        }

        memset(arg1, '\0', strlen(arg1));
        memset(arg2, '\0', strlen(arg2));
        memset(client_message, '\0', strlen(client_message));
    }

    //exit(0);
    return 0;
}



// List command
int listQuery(char* arg1, char* arg2, int lflag)
{
    char path[1024], buffer[BUF_SIZE];;
                
    DIR *dir;
    struct dirent *ent;

    // check for path argument and set path
    if(strchr(arg1, '/') != 0) //arg1[0] != 0 && 
    {
        memcpy(path, arg1, strlen(arg1));
    }
    // else set path to cwd
    else if (getcwd(path, sizeof(path)) == NULL)
    {
        perror("getcwd error : ");
        write(client_sock, "list error", 10);
        return 1;
    }

    // Try to open path directory
    if ((dir = opendir (path)) != NULL) 
    {
        int bytesSent = 0, check = 0, count = 0;
        // print all the files and directories within
        while ((ent = readdir (dir)) != NULL) 
        {
            if(count <= 40)
            {
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) 
                    continue;
                else
                {   
                    if(lflag == 1)
                    {
                        struct stat stbuf;

                        if (stat(ent->d_name, &stbuf) == -1) 
                        {
                            fprintf(stderr, "fsize: can't access %s\n", ent->d_name);
                            return 1;
                        }

                        char out[100], sizeVal[100], owner[100], modtime[100];
                        strcpy(out, ent->d_name);
                        strcat(out, " | Size: ");

                        // size
                        snprintf (sizeVal, sizeof(sizeVal), "%d", stbuf.st_size);
                        strcat(out, sizeVal);
                        strcat(out, "bytes | Owner: ");

                        // user name
                        struct passwd *pw;
                        pw = getpwuid(stbuf.st_uid);
                        if (!pw)
                        {
                            perror("struct passwd");
                        }
                        else
                        {
                            if(pw->pw_name != NULL)
                            {
                                snprintf (owner, sizeof(owner), "%s", pw->pw_name);
                                strcat(out, owner);
                            }
                        }

                        // date
                        strcat(out, " | Date: ");
                        strftime(modtime, sizeof(modtime), "%Y-%m-%d", localtime(&stbuf.st_mtime));
                        strcat(out, modtime);

                        // access permissions
                        strcat(out, (S_ISDIR(stbuf.st_mode)) ? " | Access: d" : " | Access: -");
                        strcat(out, (stbuf.st_mode & S_IRUSR) ? "r" : "-");
                        strcat(out, (stbuf.st_mode & S_IWUSR) ? "w" : "-");
                        strcat(out, (stbuf.st_mode & S_IXUSR) ? "x" : "-");
                        strcat(out, (stbuf.st_mode & S_IRGRP) ? "r" : "-");
                        strcat(out, (stbuf.st_mode & S_IWGRP) ? "w" : "-");
                        strcat(out, (stbuf.st_mode & S_IXGRP) ? "x" : "-");
                        strcat(out, (stbuf.st_mode & S_IROTH) ? "r" : "-");
                        strcat(out, (stbuf.st_mode & S_IWOTH) ? "w" : "-");
                        strcat(out, (stbuf.st_mode & S_IXOTH) ? "x ~\n" : "- ~\n\0");

                        write(client_sock, out, strlen(out));
                    }
                    else
                    {
                        write(client_sock, ent->d_name , strlen(ent->d_name));
                        write(client_sock, "\n" , 1);
                    }
                }
            }
            else
            {
                count = 0;
                char* ctnd = "\nType to next...\n";
                write(client_sock, ctnd, strlen(ctnd));

                recv(client_sock , buffer , 2000 , 0); // wait for client response
            }

            count++;

        }
        closedir (dir);
    }
    else
    {
        perror("Opening dir error : ");
        write(client_sock, "Error opening directory..\n", 26);
    }

    return 0;
}






// Get command
int getQuery(char* arg1, char* arg2)
{
    char buffer[BUF_SIZE];    
 
    // No file specified
    if(arg1[0] == 0)
    {
        write(client_sock, "fail 1.. ", 10);
        return 1;
    }


    FILE* fp = fopen(arg1, "r"); 
    if(fp == NULL)
    {
        perror("Error with filename argument..\n");
        write(client_sock, "Error with filename argument..\n", 31);
        return 1;
    } 
 
    //-- Write
    int count = 0;

    char* p;
    char line[60];
    while (p = fgets(line, 60, fp)) 
    {
        if(count <= 40)
        {
            write(client_sock, p, strlen(p));
        }
        else
        {
            count = 0;
            char* ctnd = "Type for next...\n";
            write(client_sock, ctnd, strlen(ctnd));

            recv(client_sock , buffer , 2000 , 0); // wait for client response
        }

        count++;  
    }
 
    memset(arg1, '\0', strlen(arg1));
    memset(arg2, '\0', strlen(arg2));

    fclose(fp);
    return 0;   
}




// Put command
int putQuery(char* arg1, char* arg2, int fflag)
{
    char fname[100];
    char buffer[BUF_SIZE];

    // Check if new and set file name 
    if(arg2[0] != 0)
    {
        strcpy(fname, arg2);
    }
    else
    {
        strcpy(fname, arg1);
    }


    // check if file exists
    if( access( fname, F_OK ) != -1 ) 
    {
        if(fflag != 1)
        {
            printf("Output file exists (no overwrite)\n");
            write(client_sock, "file exists", 12);
            return 1;
        }
    } 


    FILE *fp;
    fp = fopen(fname, "w");

    if(fp == NULL)
    {
        perror("Error with filename argument..");
        write(client_sock, "error", 6);

        fclose(fp);
        return 1;
    }

    write(client_sock, "ready", 6); // tell client we are ready to recv

    recv(client_sock , buffer , 2000 , 0); // recv file data

    fprintf(fp, "%s\n", buffer); // print data to file

    printf("Put query done\n" );
 
    fclose(fp);
    return 0;
}




// Delay command
void delayQuery(char* arg)
{
    int delaySeconds;
    sscanf(arg, "%d", &delaySeconds);

    if(delaySeconds > 0)
    {
        sleep(delaySeconds);
        write(client_sock, arg, strlen(arg));
    }
    else
    {
        write(client_sock, "delay time error..\n", 19);
    }

    memset(arg, '\0', strlen(arg));
}




//  System command
void sysQuery()
{
    // get/write os info
    struct utsname unameData;
    uname(&unameData);

    char sys[100];
    strcpy(sys, "System : ");
    strcat(sys, unameData.sysname);

    char ver[100];
    strcpy(ver,"\nVersion : ");
    strcat(ver, unameData.version);


    // get & write cpu info
    char out[50];
    char cpu[50];

    cpuInfo(cpu);

    strcpy(out, sys);
    strcat(out, ver);
    strcat(out, cpu);

    write(client_sock, out, strlen(out));

    // write(client_sock, cpu, strlen(cpu));
    // write(client_sock, sys, strlen(sys));
    // write(client_sock, ver, strlen(ver));
}

// Get CPU info
char* cpuInfo(char* str)
{
    char info[100];


    FILE *cpuinfo = fopen("/proc/cpuinfo", "r");

    char* key = "model name";

    char line[60];
    char *p;

    int findlen = strlen(key);


    while (p = fgets(line, 60, cpuinfo)) 
    {
        if(strstr(p, key) != NULL)
        {   
            p += (findlen + 2);

            break;
        }
    }

    fclose(cpuinfo);

    strcpy(info,"\nCPU :");
    strcat(info, p);

   // write(client_sock, info, strlen(info));
    strcpy(str, info);

    return str;
}


// On exit function
void zombieKill()
{
    // // wait for child
    if (waitpid(pid, &status, 0) < 0) 
    {
        printf("No Zombies!..\n");
    } 
}

